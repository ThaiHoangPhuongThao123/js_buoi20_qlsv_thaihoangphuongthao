function SinhVien(_id, _ten, _email, _pass, _toan, _ly, _hoa) {
  this.id = _id;
  this.ten = _ten;
  this.email = _email;
  this.pass = _pass;
  this.toan = _toan;
  this.ly = _ly;
  this.hoa = _hoa;
  this.tinhDTB = function () {
    var dtb = (this.toan + this.ly + this.hoa) / 3;
    return dtb.toFixed(2);
  };
}

