function layThongTinTuForm() {
  // lấy thông tin từ form
  var id = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var pass = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  // lưu thông tin
  var sv = new SinhVien(id, ten, email, pass, toan, ly, hoa);
  return sv;
}

function renderDS(ds) {
  var contentHTML = "";
  for (var i = 0; i < ds.length; i++) {
    var sv = ds[i];
    console.log("sv: ", sv);
    var contentRow = `
         <tr>
         <td>${sv.id}</td>
         <td>${sv.ten}</td>
         <td>${sv.email}</td>
         <td>${sv.tinhDTB()}</td>
         <td><button class = "btn btn-success" onclick = "xoaSV('${
           sv.id
         }')">Xóa</button></td>
         <td><button class = "btn btn-primary" onclick = "suaSV('${
           sv.id
         }')">Sửa</button></td>
         </tr>
         `;
    contentHTML += contentRow;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sv) {
  // lấy thông tin từ form
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.pass;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value  = sv.ly;
  document.getElementById("txtDiemHoa").value  = sv.hoa;

}
