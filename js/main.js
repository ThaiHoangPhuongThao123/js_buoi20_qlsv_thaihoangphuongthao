var dssv = [];
// Lấy thông tin lên từ local storage
var dataJSON = localStorage.getItem("DATA_DSSV");
if (dataJSON != null) {
  var arrDS = JSON.parse(dataJSON);
  for (var i = 0; i < arrDS.length; i++) {
    var item = arrDS[i];
    var sv = new SinhVien(
      item.id,
      item.ten,
      item.email,
      item.pas,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  
  }
    renderDS(dssv);
}
function themSinhVien() {
  // lấy thông tin từ form
  var sv = layThongTinTuForm();
  // lưu lại
  dssv.push(sv);
  // render thông tin
  renderDS(dssv);
  // convert dssv thành kiểu JSON
  var dataJSON = JSON.stringify( dssv);
  //   Lưu  danh sách sinh viên kiểu JSON xuống local storage
  localStorage.setItem("DATA_DSSV",dataJSON);
}
// xóa sinh viên
function xoaSV(id) {
  var viTri = dssv.findIndex(function (item) {
    return id == item.id;
  });

  if (viTri >= 0) {
    dssv.splice(viTri, 1);
    renderDS(dssv);
  }
}

function suaSV(id) {
  // vô hiệu hóa ô input-id
  document.getElementById("txtMaSV").disabled = true;
  var viTri = dssv.findIndex(function (item) {
    return id == item.id;
  });

  if (viTri >= 0) {
    // show thông tin lên form
    showThongTinLenForm(dssv[viTri]);
  }
}

function capNhatSinhVien(id) {
  // lấy ra siinh viên đã chỉnh sửa:
  var sv = layThongTinTuForm();
  // Tìm vị trí cần cập nhập;
  var viTri = dssv.findIndex(function (item) {
    return sv.id == item.id;
  });
  if (viTri >= 0) {
    dssv[viTri] = sv;
    renderDS(dssv);
    resetDSSV();
  }
}

function resetDSSV() {
  document.getElementById("formQLSV").reset();
}
